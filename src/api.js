// window.addEventListener('pywebviewready', function() {
//     var container = document.getElementById('pywebview-status')
//     container.innerHTML = '<i>pywebview</i> is ready'
// })


function toggleBar() {
    bars = document.getElementsByClassName("sidebar");
    for (i in bars) {
        let bar = bars[i]
        out = bar.getAttribute("data-hidden");
        if (out == "") {
            bar.setAttribute("data-hidden",'false');
        } else if (out == "true") {
            bar.setAttribute('data-hidden', 'false');
        } else {
            bar.setAttribute('data-hidden', 'true');
        }
    }
}

function buildSongs() {
    function ihatejs(songs) {
        if (songs != null) {
        item = document.getElementById("queuelist");
        var out = "";
        for (let i in songs) {
            song = songs[i]
            // pywebview.api.print(String(song));
            // pywebview.api.print(song);
            // console.log(song);
            out += '<li class="songlist_item" data-song-index=+'+i+'>';
            out += '<p class="songlist_title">'+song['title']+'</p>';
            out += '<p class="songlist_artist">'+song['artist']+'</p>';
            out += "</li>"
        }
        // console.log(out);
        // pywebview.api.print("hows it goin",out);
        item.innerHTML = out;

    }}
    // alert("hi")
    pywebview.api.getSongs().then(ihatejs);
    // pywebview.api.print(songs)
    // pywebview.api.print(songs);

    // pywebview.api.print("hi")
    // for (let i in songs) {
    //     out += "<li>"+songs[i]+"</li>"
    // }
    // console.log(out)
    // pywebview.api.print("hows it goin",out);
    // item.innerHTML = out;
}

function clickPress(event) {
    if (event.keyCode == 13) {
        addSong();
    }}

function skipSong() {
    pywebview.api.skip();
}

function setall(thebeans, attr, bingo) {
    for (i in thebeans) {
        thebeans.setAttribute(attr, bingo);
    }
}

function setallhtml(thebeans, html) {
    for (i in thebeans) {
        thebeans[i].innerHTML = html;
    }
}


function prevSong() {
    pywebview.api.prev();
}

function togglePause() {
    pywebview.api.togglePause();
}

function updateUI() {
    function doit(song) {
        console.log(song)
        if (song != null) {
            document.getElementById("song-image").setAttribute('src', song['thumbnail'])
            // document.getElementById("song-bg-image").setAttribute('src', song['thumbnail'])
            document.getElementById("song-bg-image").setAttribute('style', 'background-image: url("'+song['thumbnail']+'");')

            setallhtml(document.getElementsByClassName("current-song-title"), song["title"]);
            setallhtml(document.getElementsByClassName("current-song-artist"), song["artist"]);
            
            buildSongs();
        } else {
            document.getElementById("song-bg-image").setAttribute('style', 'background-image: url("'+'pytermlogohalfbg'+'");')
            document.getElementById("song-image").setAttribute('src', 'pytermlogohalfbg.png')

            setallhtml(document.getElementsByClassName("current-song-title"), "nothing playing");
            setallhtml(document.getElementsByClassName("current-song-artist"), "");
        }
    }

    pywebview.api.getCurrentSong().then(doit);
}

function addSong() {
    let name = document.getElementById("searchbar");
    pywebview.api.addSong(name.value);
    name.value = "";
}