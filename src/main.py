import webview
import pYTerm
import time
import traceback

try:
    from pYTermLink import pYTerm
    print('using dev pYTerm')
except ImportError:
    from pYTerm import pYTerm


def dictify_song(song):
    if song:
        a = vars(song).copy()
        del a['pafyobj']
        return a

class Api():
    def __init__(self, player: pYTerm.Player):
        self.player = player
        self.pause = self.player.pause
        self.prev = self.player.previous
        self.unpause = self.player.unpause
        self.skip = self.player.next
        self.getPaused = self.player.is_paused
        self.setVolume = lambda x: self.player.set_volume(x, fadetime=0)
        self.getProgress = self.player.get_progress
        self.print = print
        self.togglePause = self.player.toggle_pause

    def getSongs(self):
        songs = self.player.get_songs()
        if len(songs) == 0:
            print('no songs oops')
            return
        out = [dictify_song(song) for song in songs]
        # time.sleep(1)
        # response = {
        #     'message': out
        # }
        # traceback.print_exc()
        return out

    def getCurrentSong(self):
        song = self.player.current_song
        print('csong:',song)
        return dictify_song(song)
    

    def addSong(self, song):
        self.player.add_song(song)
        print(self.player.songs[-1])
        print(vars(self.player.songs[-1]))
        # print(self.player.is_alive())

def main():
    with open('app.html', 'r') as f:
        html = f.read()
    with open('api.js', 'r') as f:
        html = html.replace('$$$js','<script> '+f.read()+'</script>')
    with open('main.css', 'r') as f:
        html = html.replace('$$$css','<style> '+f.read()+'</style>')
    player = pYTerm.Player(enable_input=False, songs="mother mother o my heart", shuffle=True, debuglogs=True)
    player.play_all(keep_alive=True, halting=False)
    api = Api(player)
    window = webview.create_window('pYTerm', html=html, transparent=False, js_api=api)
    webview.start(debug=True)


if __name__ == '__main__':
    main()